package communication;

import communication.Communications.CommunicationsTuple;

import better.org.json.JSONException;
import better.org.json.JSONObject;
import android.util.Log;

/*
 * @author Kevin Michael Amaral
 */

public class Authentication {
    private String email;
    private String password;
    
    private Authentication() {}
    
    public Authentication(String email, String password) {
        this();
        this.email = email;
        this.password = password;
    }
    
    public JSONObject authenticate(String target) {
        // Create credential object
        JSONObject credentials = new JSONObject();
        try {
            credentials.put("password", password);
            credentials.put("email", email);
        } catch (JSONException e) {
            Log.e("Authentication.authenicate", e.toString());
        }
        
        // Wrap in user object
        JSONObject userObject = new JSONObject();
        try {
            userObject.put("user", credentials);
        } catch (JSONException e) {
            Log.e("Authentication.authenicate", e.toString());
        }
        
        // Send user object to target server and get response
        CommunicationsTuple response = Communications.post(userObject, target);
        
        // If failed to receive HTTP Response, return null
        if (response == null)
            return null;

        // Extract HTTP Status Code
        int statusCode = response.getStatusCode();
        
        JSONObject token = null;
        switch(statusCode) {
            case 401:
            case 200:
                // Extract content from HTTP Response.
                String content = response.getContent();

                // Interpret content as JSON Object
                try {
                    token = new JSONObject(content);
                } catch (JSONException e) {
                    Log.e("Authentication.authenicate", e.toString());
                }
                
                // If failed to construct JSON Object, return null
                return token;
            default:
                // On other Status Codes, fall through to the end
                break;
        }
        
        // Returns null on overall failure
        return null;
    }
}