package communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;

import better.org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

/*
 * @author Kevin Michael Amaral
 */

public class Communications {
    private static final HttpClient client = new DefaultHttpClient();
    
    private Communications() {}
    
    public static synchronized CommunicationsTuple post(Object letter, String target) {
        // Prepare to perform request in an AsyncTask
        CommunicationsTask requestTask = new CommunicationsTask();
        
        // Receive response from AsyncTask
        CommunicationsTuple postResponse = null;
        try {
            postResponse = requestTask.execute(HttpPost.class, target, letter).get();
        } catch (InterruptedException e) {
            Log.e("Communications.post", e.toString());
        } catch (ExecutionException e) {
            Log.e("Communications.post", e.toString());
        }
        
        // On failure, return null
        return postResponse;
    }
    
    public static synchronized CommunicationsTuple get(String target) {
        // Prepare to perform request in an AsyncTask
        CommunicationsTask requestTask = new CommunicationsTask();

        // Receive response from AsyncTask
        CommunicationsTuple getResponse = null;
        try {
            getResponse = requestTask.execute(HttpGet.class, target).get();
        } catch (InterruptedException e) {
            Log.e("Communications.get", e.toString());
        } catch (ExecutionException e) {
            Log.e("Communications.get", e.toString());
        }
        
        // On failure, return null
        return getResponse;
    }

    
    /*
     * The inputs of a CommunicationsTask are java.lang.Class, java.lang.String, and java.lang.Object
     * The first input (java.lang.Class) should be the class of the HTTP Request you want to send.
     * 		Normally, HttpGet.class or HttpPost.class. Nothing else is currently supported.
     * The second input (java.lang.String) should be the target webserver's address.
     * The third input (java.lang.Object), is neglected for HTTP Get Requests.
     * 		It is the content appended to the end of the request to be given to the server.
     * 		Normally, HTTP Post data.
     */
    public static class CommunicationsTask extends AsyncTask<Object, Void, CommunicationsTuple> {
        protected CommunicationsTuple doInBackground(Object... inputs) {
            HttpRequestBase request = null;
            
            if(inputs[0] == HttpGet.class) {
            	request = new HttpGet((String) inputs[1]);
            } else if (inputs[0] == HttpPost.class) {
            	request = new HttpPost((String) inputs[1]);
            	
            	if (inputs[2] instanceof JSONObject) {
                    request.addHeader("Content-Type", "application/json");
                } else {
                    request.addHeader("Content-Type", "text/plain");
                }
                
                // Attach JSON Object to Request
                StringEntity letterEntity = null;
                try {
                    letterEntity = new StringEntity(inputs[2].toString());
                } catch (UnsupportedEncodingException e) {
                    Log.e("Communications.post", e.toString());
                } finally {
                    if (letterEntity == null)
                        return null;
                }
                ((HttpPost) request).setEntity(letterEntity);
            } else {
            	return null;
            }
            
            HttpResponse response = null;
            try {
                response = client.execute(request);
            } catch (ClientProtocolException e) {
                Log.e("CommunicationsTask.execute", e.toString());
            } catch (IOException e) {
                Log.e("CommunicationsTask.execute", e.toString());
            }
            
            // return null on failure to get response
            if (response == null)
                return null;
            
            int statusCode = response.getStatusLine().getStatusCode();
            String content = "";
            
            InputStream contentStream = null;
            try {
                contentStream = response.getEntity().getContent();
            } catch (IllegalStateException e) {
                Log.e("Authentication.authenicate", e.toString());
            } catch (IOException e) {
                Log.e("Authentication.authenicate", e.toString());
            }
            
            // return null on failure to get stream
            if (contentStream == null)
            	return null;
            
            Scanner contentScanner = new Scanner(contentStream).useDelimiter("\\A");
            if (contentScanner.hasNext())
            	content = contentScanner.next();
            
            CommunicationsTuple tuple = new CommunicationsTuple(statusCode, content);
            
            return tuple;
        }
    }
    
    public static class CommunicationsTuple {
    	private Integer statusCode;
    	private String content;
    	
    	private CommunicationsTuple() {}
    	public CommunicationsTuple(Integer statusCode, String content) {
    		this();
    		this.statusCode = statusCode;
    		this.content = content;
    	}
    	
    	public Integer getStatusCode() {
    		return statusCode;
    	}
    	
    	public String getContent() {
    		return content;
    	}
    }
}