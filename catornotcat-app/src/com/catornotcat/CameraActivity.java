package com.catornotcat;


import com.wilson.android.library.DrawableManager;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ImageView;

public class CameraActivity extends Activity {
	private DrawableManager drawableManager;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        drawableManager = new DrawableManager();
        ImageView imageView1 = (ImageView) findViewById(R.id.imageView2);
        imageView1.setImageDrawable(drawableManager.fetchDrawable("http://www.dotacinema.com/images_news/202_image_730x300.jpg"));
        //drawableManager.fetchDrawableOnThread("http://www.dotacinema.com/images_news/202_image_730x300.jpg",imageView1);
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_cat_camera, menu);
        return true;
    }
}
