package com.catornotcat;

import java.util.LinkedList;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.catornotcat.R;
import com.wilson.android.library.DrawableManager;

import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import communication.Communications;
import communication.Communications.CommunicationsTuple;

public class CatOrNotCatActivity3 extends Activity {
	private DrawableManager drawableManager;
	private LinkedList<Integer> idList;
	private int currentCat;
	private JSONObject currentCatJSON;
	private TextView yesVotes;
	private TextView noVotes;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cat_or_not_cat);
        idList = new LinkedList<Integer>();
        drawableManager = new DrawableManager();
        String indexUrl = "http://www.catornotcat.com/cats.json";
        CommunicationsTuple indexTuple = Communications.get(indexUrl);
        JSONArray indexJSONArray = null;
        try {
			indexJSONArray = new JSONArray(indexTuple.getContent());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        JSONObject jobject = null;
        for(int i = 0; i < indexJSONArray.length();i++)
        {
        	try {
				jobject = indexJSONArray.getJSONObject(i);
				idList.addLast((int) jobject.getLong("id"));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
        yesVotes = (TextView) findViewById(R.id.txt_yesVotes);
        noVotes = (TextView) findViewById(R.id.txt_noVotes);
        displayCat();
        
    }
    public void cat(View view)
    {
      	yesVotes.setText("");
    	noVotes.setText("");
    	StringBuilder catURLYes = new StringBuilder("http://www.catornotcat.com/cats/");
    	catURLYes.append(currentCat);
    	catURLYes.append("/yes/");
    	Communications.get(catURLYes.toString());
        displayCat();
    }
    public void notCat(View view)
    {
    	yesVotes.setText("");
    	noVotes.setText("");
    	
    	StringBuilder catURLNo = new StringBuilder("http://www.catornotcat.com/cats/");
    	catURLNo.append(currentCat);
    	catURLNo.append("/no/");
    	Communications.get(catURLNo.toString());
        displayCat();
    }
    
    public void revealVotes (View view)
    {
    	StringBuilder cat = new StringBuilder("Cat: ");
    	StringBuilder notCat = new StringBuilder("Not Cat: ");
    	String yVotes = null;
    	String nVotes = null;
    	try {
    		yVotes = currentCatJSON.getString("yes");
			nVotes = currentCatJSON.getString("no");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	cat.append(yVotes);
    	notCat.append(nVotes);
    	
    	yesVotes.setText(cat);
    	noVotes.setText(notCat);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_android_cat_or_not_cat, menu);
        return true;
    }
    @TargetApi(9)
	private void displayCat()
    {
     	  StringBuilder catJSONURL = new StringBuilder("http://www.catornotcat.com/cats/");
     	  if(!idList.isEmpty())
     		  currentCat = idList.pop();
          
     	  catJSONURL.append(currentCat);
          catJSONURL.append(".json");
          CommunicationsTuple catJSONTuple = Communications.get(catJSONURL.toString());
          try {
        	  currentCatJSON = new JSONObject(catJSONTuple.getContent());
    		} catch (JSONException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
          StringBuilder imageURL = new StringBuilder("http://www.catornotcat.com");
          try {
    			imageURL.append(currentCatJSON.getString("cat_url"));
    		} catch (JSONException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
          ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
          drawableManager.fetchDrawableOnThread(imageURL.toString(),imageView1);
          

    }
}
