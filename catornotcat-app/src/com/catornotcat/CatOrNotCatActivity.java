package com.catornotcat;

import java.util.LinkedList;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.catornotcat.R;
import com.wilson.android.library.DrawableManager;

import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import communication.Communications;
import communication.Communications.CommunicationsTuple;

public class CatOrNotCatActivity extends Activity {
	private DrawableManager drawableManager;
	private LinkedList<Integer> idList;
	private int currentCat;
	private JSONObject currentCatJSON;
	private Button  butCat;
	private Button  butNotCat;
	private ProgressBar pb;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cat_or_not_cat_activity2);
		idList = new LinkedList<Integer>();
		drawableManager = new DrawableManager();
		String indexUrl = "http://www.catornotcat.com/cats.json";
		CommunicationsTuple indexTuple = Communications.get(indexUrl);
		JSONArray indexJSONArray = null;
		try {
			indexJSONArray = new JSONArray(indexTuple.getContent());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jobject = null;
		for(int i = 0; i < indexJSONArray.length();i++)
		{
			try {
				jobject = indexJSONArray.getJSONObject(i);
				idList.addLast((int) jobject.getLong("id"));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		butCat = (Button) findViewById(R.id.but_yes);
		butNotCat = (Button) findViewById(R.id.but_no);
		pb = (ProgressBar) findViewById(R.id.ProgressBar01);
		displayCat();

	}
	public void cat(View view)
	{
		//yesVotes.setText("");
		//noVotes.setText("");
		StringBuilder catURLYes = new StringBuilder("http://www.catornotcat.com/cats/");
		catURLYes.append(currentCat);
		catURLYes.append("/yes/");
		Communications.get(catURLYes.toString());
		pb.setVisibility(0);
		revealVotes ();
	}
	public void notCat(View view)
	{
		//yesVotes.setText("");
		//noVotes.setText("");

		StringBuilder catURLNo = new StringBuilder("http://www.catornotcat.com/cats/");
		catURLNo.append(currentCat);
		catURLNo.append("/no/");
		Communications.get(catURLNo.toString());
		pb.setVisibility(0);
		revealVotes ();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_android_cat_or_not_cat, menu);
		return true;
	}
	public void nextCat(View view)
	{
		butCat.setText("Cat");
		butNotCat.setText("Not Cat");
		pb.setProgress(0);
		displayCat();
	}
	@TargetApi(9)
	private void displayCat()
	{
		StringBuilder catJSONURL = new StringBuilder("http://www.catornotcat.com/cats/");
		if(!idList.isEmpty())
			currentCat = idList.pop();

		catJSONURL.append(currentCat);
		catJSONURL.append(".json");
		CommunicationsTuple catJSONTuple = Communications.get(catJSONURL.toString());
		try {
			currentCatJSON = new JSONObject(catJSONTuple.getContent());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StringBuilder imageURL = new StringBuilder("http://www.catornotcat.com");
		try {
			imageURL.append(currentCatJSON.getString("cat_url"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ImageView imageView2 = (ImageView) findViewById(R.id.imageView2);
		drawableManager.fetchDrawableOnThread(imageURL.toString(),imageView2);
		pb.setVisibility(4);
		butCat.setEnabled(true);
		butNotCat.setEnabled(true);


	}

	public void revealVotes ()
	{
		StringBuilder cat = new StringBuilder("Cat: ");
		StringBuilder notCat = new StringBuilder("Not Cat: ");
		String yVotes = null;
		String nVotes = null;
		double douYes = 0;
		double douNoes = 0;
		try {
			StringBuilder catJSONURL = new StringBuilder("http://www.catornotcat.com/cats/");
			catJSONURL.append(currentCat);
			catJSONURL.append(".json");
			CommunicationsTuple catJSONTuple = Communications.get(catJSONURL.toString());
			currentCatJSON = new JSONObject(catJSONTuple.getContent());
			yVotes = currentCatJSON.getString("yes");
			nVotes = currentCatJSON.getString("no");
			douYes = currentCatJSON.getDouble("yes");
			douNoes = currentCatJSON.getDouble("no");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if((douYes + douNoes) != 0)
		{
			double ratio = douYes/(douYes + douNoes);
			ratio *= 100;
			pb.setProgress((int) ratio);
		}
		cat.append(yVotes);
		notCat.append(nVotes);
		butCat.setEnabled(false);
		butNotCat.setEnabled(false);
		butCat.setText(cat);
		butNotCat.setText(notCat);
	}
}
